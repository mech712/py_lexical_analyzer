import compil.lexer_p.lexer as lexer
import compil.lexer_p.token as token
import compil.inter.id as id

class Env:
    table = {}
    prev = None

    def __init__(self, n):
        self.table = {}
        self.prev = n

    def put(self, w:token.Token, i:id.Id):
        self.table[w] = i

    def get(self, w:token.Token)->id.Id:
        e = self
        while e is not None:
            found = e.table.get(w)
            if found is not None: return found

            e = e.prev
        return None

