from compil.symbols_p.type import Type
from compil.lexer_p.tag import Tag


class Array(Type):
    size = 1
    
    def __init__(self, sz:int, p:Type):
        super().__init__('[]', Tag.INDEX_, sz * p.width)
        self.of = p
        self.size = sz

    def __str__(self):
        return f"[{self.size}] {str(self.of)}"
    