import compil.lexer_p.word as word
import compil.lexer_p.tag as tag


class Type(word.Word):
    width:int=0

    def __init__(self, s:str, tag:int, w:int):
        super().__init__(s, tag)
        self.width = w


    @staticmethod
    def numeric(p)->bool:
        global Int, Float, Char, Bool
        if p == Char or p == Int or p == Float:
            return True
        return False

    @staticmethod
    def max(p1, p2):
        global Int, Float, Char, Bool
        if not Type.numeric(p1) or not Type.numeric(p2): return None
        if p1 == Float or p2 == Float: return Float
        if p1 == Int or p2 == Int: return Int
        return Char



Int = Type('int', tag.Tag.BASIC_, 4)
Float = Type('float', tag.Tag.BASIC_, 8)
Char = Type('char', tag.Tag.BASIC_, 1)
Bool = Type('bool', tag.Tag.BASIC_, 1)