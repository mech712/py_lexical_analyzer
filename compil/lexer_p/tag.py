

class Tag:
    AND_ = 256
    ELSE_ = 260
    ID_  = 264
    MINUS_ = 268
    REAL_ = 272
    BASIC_ = 257
    EQ_ = 261
    IF_ = 265
    NE_ = 269
    TEMP_ = 273
    BREAK_ = 258
    FALSE_ = 262
    INDEX_ = 266
    NUM_ = 270
    TRUE_ = 274
    DO_ = 259
    GE_ = 263
    LE_ = 267
    OR_ = 271
    WHILE_ = 275