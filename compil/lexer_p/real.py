import compil.lexer_p.tag as tag
import compil.lexer_p.token as token

class Real(token.Token):
    def __init__(self, v:float):
        super().__init__(tag.Tag.REAL_)
        self.value = v

    def __str__(self):
        return f"{str(self.value)} : {self.tag}"