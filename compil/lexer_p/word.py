import compil.lexer_p.tag as tag
import compil.lexer_p.token as token

class Word(token.Token):
    def __init__(self, s:str, tag:int):
        super().__init__(tag)
        self.lexeme = s

    def __str__(self):
        return f"{self.lexeme} : {self.tag}"

and_ = Word('&&', tag.Tag.AND_)
or_ = Word('||', tag.Tag.OR_)
eq_ = Word('==', tag.Tag.EQ_)
ne_ = Word('!=', tag.Tag.NE_)
le_ = Word('<=', tag.Tag.LE_)
ge_ = Word('>=', tag.Tag.GE_)
minus_ = Word('minus', tag.Tag.MINUS_)
True_ = Word('true', tag.Tag.TRUE_)
False_ = Word('false', tag.Tag.FALSE_)
temp_ = Word('t', tag.Tag.TEMP_)
