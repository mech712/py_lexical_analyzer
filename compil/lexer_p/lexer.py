import sys

import compil.lexer_p.word as word
import compil.lexer_p.tag as tag
import compil.lexer_p.num as num
import compil.lexer_p.real as real
import compil.lexer_p.token as token
import compil.symbols_p.type as type_


class Input():
    def __init__(self, *args, **kwargs):
        self.buffer = "int a = 10; a = 3 + a;"
        self.index = 0
    
    def read(self, i):
        if self.index < len(self.buffer):
            res = self.buffer[self.index]
            self.index += 1
            return res
        return ''

class Lexer:
    line:int = 1
    peek = ' ' 
    words = {}
    input_ = sys.stdin

    def reserve(self, w:word.Word):
        self.words[w.lexeme] = w

    def __init__(self, inp=None):
        if inp: self.input_ = inp
        self.reserve(word.Word('if', tag.Tag.IF_))
        self.reserve(word.Word('else', tag.Tag.ELSE_))
        self.reserve(word.Word('while', tag.Tag.WHILE_))
        self.reserve(word.Word('do', tag.Tag.DO_))
        self.reserve(word.Word('break', tag.Tag.BREAK_))
        self.reserve(word.True_)
        self.reserve(word.False_)
        self.reserve(type_.Int)
        self.reserve(type_.Char)
        self.reserve(type_.Bool)
        self.reserve(type_.Float)

    def readch(self, c=None):
        if c is None:
            self.peek = self.input_.read(1)
        else:
            self.readch()
            if self.peek != c:
                return False
            self.peek = ' '
            return True

    
    def scan(self):
        while self.readch():
            if self.peek == ' ' or self.peek == '\t': continue
            elif self.peek == '\n': self.line += 1
            else: break
        
        if self.peek == '&':
            if self.readch('&'): return word.and_
            else: return token.Token('&')

        elif self.peek == '|':
            if self.readch('|'): return word.or_
            else: return token.Token('|')

        elif self.peek == '=':
            if self.readch('='): return word.eq_
            else: return token.Token('=')

        elif self.peek == '!':
            if self.readch('='): return word.ne_
            else: return token.Token('!')

        elif self.peek == '<':
            if self.readch('='): return word.le_
            else: return token.Token('<')

        elif self.peek == '>':
            if self.readch('='): return word.ge_
            else: return token.Token('>')

        
        if self.peek.isdigit():
            v = 0
            while self.peek.isdigit():
                v = 10 * v + int(self.peek)
                self.readch()
            if self.peek != '.': return num.Num(v)
            x = float(v)
            d = 10.0
            while True:
                self.readch()
                if not self.peek.isdigit(): break
                x = x + float(self.peek) / d
                d *= 10
            return real.Real(x)

        if self.peek.isalpha():
            b = ''
            while True:
                b = b + self.peek
                self.readch()
                if not(self.peek.isdigit() or self.peek.isalpha()): break
            w = self.words.get(b)
            if w is not None: return w 
            w = word.Word(b, tag.Tag.ID_)
            self.words[b] = w
            return w
        
        tok = token.Token(self.peek)
        self.peek = ' '
        return tok
