import compil.lexer_p.tag as tag
import compil.lexer_p.token as token

class Num(token.Token):
    def __init__(self, v:int):
        super().__init__(tag.Tag.NUM_)
        self.value = v

    def __str__(self):
        return f"{str(self.value)} : {self.tag}"