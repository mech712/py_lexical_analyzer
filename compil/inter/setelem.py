import compil.symbols_p.type as type_
import compil.symbols_p.array as array

import compil.inter.expr as expr
import compil.inter.stmt as stmt
import compil.inter.id as id
import compil.inter.access as access


class SetElem(stmt.Stmt):

    def __init__(self, x:access.Access, y:expr.Expr):
        self.array:id.Id = x.array
        self.index:expr.Expr = x.index
        self.expr:expr.Expr = y
        if self.check(x.type_, expr.type_) is None:
            self.error('type error')

    def check(self, p1:type_.Type, p2:type_.Type)->type_.Type:
        if isinstance(p1, array.Array) or isinstance(p2, array.Array):
            return None
        elif p1 == p2:
            return p2
        elif type_.Type.numeric(p1) and type_.Type.numeric(p2):
            return p2
        return None

    def gen(self, b:int, a:int):
        s1:str = str(self.index.reduce())
        s2:str = str(self.expr.reduce())
        self.emit(f"{str(self.array)} [ {s1} ] = {s2}")

