import compil.inter.expr as expr
import compil.lexer_p.token as token
import compil.inter.logical as logical


class Or(logical.Logical):

    def __init__(self, tok:token.Token, x1:expr.Expr, x2:expr.Expr):
        super().__init__(tok, x1, x2)
        
    def jumping(self, t:int, f:int):
        label:int = t if t != 0 else self.newlabel()
        self.expr1.jumping(label, 0)
        self.expr2.jumping(t, f)
        if t == 0:
            self.emitlabel(label)