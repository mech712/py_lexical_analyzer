import compil.inter.expr as expr
import compil.lexer_p.token as token
import compil.symbols_p.type as type_
import compil.lexer_p.word as word
import compil.lexer_p.num as num

class Constant(expr.Expr):
    def __init__(self, i:int=None,tok:token.Token=None, p:type_.Type=None):
        if i is None:
            super().__init__(tok, p)
        else:
            super().__init__(num.Num(i), type_.Int)

    def jumping(self, t:int, f:int):
        if self == True_ and t!=0:
            self.emit(f"goto L{t}")
        if self == False_ and f!=0:
            self.emit(f"goto L{f}")

True_ = Constant(word.True_, type_.Bool)
False_ = Constant(word.False_, type_.Bool)
