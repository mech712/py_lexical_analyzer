import compil.inter.expr as expr
import compil.lexer_p.token as token
import compil.symbols_p.type as type_
import compil.symbols_p.array as array
import compil.inter.logical as logical


class Rel(logical.Logical):

    def __init__(self, tok:token.Token, x1:expr.Expr, x2:expr.Expr):
        super().__init__(tok, x1, x2)
    
    def check(self, p1:type_.Type, p2:type_.Type):
        if isinstance(p1, array.Array) or isinstance(p2, array.Array):
            return None
        elif p1 == p2:
            return type_.Bool
        else:
            return None
    
    def jumping(self, t:int, f:int):
        a:expr.Expr = self.expr1.reduce()
        b:expr.Expr = self.expr2.reduce()
        test:str = f"{str(a)} {str(self.op)} {str(b)}"
        self.emitjumps(test, t, f)