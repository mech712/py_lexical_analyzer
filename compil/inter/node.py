

class Node:
    lexline:int = 0
    labels:int = 0

    def __init__(self, lex_line:int):
        self.lexline = lex_line

    def error(self, s:str):
        raise Exception(f"near line {self.lexline}: {s}")
        
    def newlabel(self)->int:
        Node.labels += 1
        return Node.labels

    def emitlabel(self, i:int):
        print(f"L{i}:")

    def emit(self, s:str):
        print(f"\t{s}")