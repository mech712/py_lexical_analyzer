import compil.inter.op as op
import compil.lexer_p.token as token
import compil.inter.expr as expr
import compil.symbols_p.type as type_

class Arith(op.Op):
    def __init__(self, tok:token.Token, x1:expr.Expr, x2:expr.Expr):
        super().__init__(tok, None)
        self.expr1 = x1
        self.expr2 = x2
        self.type_ = type_.Type.max(self.expr1.type_, self.expr2.type_)
        if self.type_ is None:
            self.error("type error")

    def gen(self)->expr.Expr:
        return Arith(self.op, self.expr1.reduce(), self.expr2.reduce())

    def __str__(self):
        return f"{str(self.expr1)} {str(self.op)} {str(self.expr2)}"