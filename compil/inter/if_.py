import compil.symbols_p.type as type_

import compil.inter.expr as expr
import compil.inter.stmt as stmt

class If(stmt.Stmt):
    
    def __init__(self, x:expr.Expr, s:stmt.Stmt):
        self.expr = x
        self.stmt = s
        if self.expr.type_ != type_.Bool:
            self.expr.error("boolean required in if")
        
    def gen(self, b:int, a:int):
        label:int = self.newlabel()
        self.expr.jumping(0, a)
        self.emitlabel(label)
        self.stmt.gen(label, a)
