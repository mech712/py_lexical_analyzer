import compil.inter.stmt as stmt


class Seq(stmt.Stmt):
    
    def __init__(self, s1:stmt.Stmt, s2:stmt.Stmt):
        self.stmt1:stmt.Stmt = s1
        self.stmt2:stmt.Stmt = s2

    def gen(self, b:int, a:int):
        if self.stmt1 == stmt.Null:
            self.stmt2.gen(b, a)
        elif self.stmt2 == stmt.Null:
            self.stmt1.gen(b, a)
        else:
            label:int = self.newlabel()
            self.stmt1.gen(b, label)
            self.emitlabel(label)
            self.stmt2.gen(label, a)
