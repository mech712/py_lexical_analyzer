import compil.inter.expr as expr
import compil.lexer_p.token as token
import compil.symbols_p.type as type_
import compil.inter.temp as temp


class Logical(expr.Expr):

    def __init__(self, tok:token.Token, x1:expr.Expr, x2:expr.Expr):
        super().__init__(tok, None)
        self.expr1 = x1
        self.expr2 = x2
        self.type_ = self.check(self.expr1.type_, self.expr2.type_)
        if self.type_ is None:
            self.error('type error')

    def check(self, p1:type_.Type, p2:type_.Type)->type_.Type:
        if p1 == type_.Bool and p2 == type_.Bool:
            return type_.Bool
        return None

    def gen(self)->expr.Expr:
        f = self.newlabel()
        a = self.newlabel()
        tmp = temp.Temp(self.type_)
        self.jumping(0, f)
        self.emit(f"{str(temp)} = true")
        self.emit(f"goto L{a}")
        self.emitlabel(f)
        self.emit(f"{str(temp)} = false")
        self.emitlabel(a)
        return tmp

    def __str__(self):
        return f"{str(self.expr1)} {str(self.op)} {str(self.expr2)}"
