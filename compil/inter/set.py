import compil.symbols_p.type as type_

import compil.inter.expr as expr
import compil.inter.stmt as stmt
import compil.inter.id as id


class Set(stmt.Stmt):
    
    def __init__(self, i:id.Id, x:expr.Expr):
        self.id = i
        self.expr = x
        if self.check(self.id.type_, self.expr.type_) is None:
            self.error("type error")

    def check(self, p1:type_.Type, p2:type_.Type)->type_.Type:
        if type_.Type.numeric(p1) and type_.Type.numeric(p2):
            return p2
        elif p1 == type_.Bool and p2 == type_.Bool:
            return p2
        return None

    def gen(self, b:int, a:int):
        self.emit(f"{str(self.id)} = {str(self.expr.gen())}")

