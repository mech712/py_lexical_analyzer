import compil.lexer_p.token as token
import compil.symbols_p.type as type_
import compil.inter.node as node

class Expr(node.Node):

    def __init__(self, tok:token.Token, p:type_.Type):
        self.op = tok
        self.type_ = p

    def gen(self):
        return self

    def reduce(self):
        return self

    def jumping(self, t:int, f:int):
        self.emitjumps(str(self), t, f)

    def emitjumps(self, test:str, t:int, f:int):
        if t != 0 and f != 0:
            self.emit(f"if {test} goto L{t}")
            self.emit(f"goto L{f}")
        elif t != 0:
            self.emit(f"if {test} goto L{t}")
        elif f != 0:
            self.emit(f"iffalse {test} goto L{f}")
        else:
            pass
    
    def __str__(self):
        return f"{str(self.op)}"