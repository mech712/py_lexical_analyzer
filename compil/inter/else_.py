import compil.symbols_p.type as type_

import compil.inter.expr as expr
import compil.inter.stmt as stmt

class Else(stmt.Stmt):
    
    def __init__(self, x:expr.Expr, s1:stmt.Stmt, s2:stmt.Stmt):
        self.expr = x
        self.stmt1 = s1
        self.stmt2 = s2
        if self.expr.type_ != type_.Bool:
            self.expr.error("boolean required in if")
        
    def gen(self, b:int, a:int):
        label1:int = self.newlabel()
        label2:int = self.newlabel()

        self.expr.jumping(0, label2)
        self.emitlabel(label1)
        self.stmt1.gen(label1, a)

        self.emit(f"goto L{str(a)}")
        self.emitlabel(label2)
        self.stmt2.gen(label2, a)
