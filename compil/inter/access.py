import compil.inter.op as op
import compil.inter.expr as expr
import compil.symbols_p.type as type_
import compil.lexer_p.word as word
import compil.lexer_p.tag as tag
import compil.inter.id as id



class Access(op.Op):

    def __init__(self, a:id.Id, i:expr.Expr, p:type_.Type):
        super().__init__(word.Word('[]', tag.Tag.INDEX_), p)
        self.array:id.Id = a
        self.index:expr.Expr = i

    def gen(self)->expr.Expr:
        return Access(self.array, self.index.reduce(), self.type_)
    
    def jumping(self, t:int, f:int):
        self.emitjumps(str(self.reduce()), t, f)

    def __str__(self):
        return f"{str(self.array)} [{str(self.index)}]"
