import compil.inter.op as op
import compil.inter.expr as expr
import compil.lexer_p.token as token
import compil.inter.logical as logical


class Not(logical.Logical):

    def __init__(self, tok:token.Token, x2:expr.Expr):
        super().__init__(tok, x2, x2)
        
    def jumping(self, t:int, f:int):
        self.expr2.jumping(f, t)

    def __str__(self):
        return f"{str(self.op)} {str(self.expr2)}"