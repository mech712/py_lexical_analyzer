import compil.inter.expr as expr
import compil.symbols_p.type as type_
import compil.lexer_p.word as word

class Temp(expr.Expr):
    count:int = 0
    number:int = 0

    def __init__(self, p:type_.Type):
        super().__init__(word.temp_, p)
        Temp.count += 1
        self.number = self.count

    def __str__(self):
        return f"t{str(self.number)}"