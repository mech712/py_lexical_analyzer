import compil.inter.stmt as stmt


class Break(stmt.Stmt):
    
    def __init__(self):
        if stmt.Enclosing is None:
            self.error('unenclosed break')
        self.stmt:stmt.Stmt = stmt.Enclosing

    def gen(self, b:int, a:int):
        self.emit(f"goto L{str(self.stmt.after)}")
