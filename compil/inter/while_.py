import compil.symbols_p.type as type_

import compil.inter.expr as expr
import compil.inter.stmt as stmt

class While(stmt.Stmt):
    
    def __init__(self):
        self.expr = None
        self.stmt = None

    def init(self, x:expr.Expr, s:stmt.Stmt):
        self.expr = x
        self.stmt = s
        if self.expr.type_ != type_.Bool:
            self.expr.error("boolean required in while")
        
    def gen(self, b:int, a:int):
        self.after = a
        self.expr.jumping(0, a)
        label:int = self.newlabel()
        self.emitlabel(label)
        self.stmt.gen(label, b)
        self.emit(f"goto L{str(b)}")
