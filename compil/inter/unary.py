import compil.inter.op as op
import compil.inter.expr as expr
import compil.lexer_p.token as token
import compil.symbols_p.type as type_

class Unary(op.Op):
    def __init__(self, tok:token.Token, x:expr.Expr):
        super().__init__(tok, None)
        self.expr = x
        self.type_ = type_.Type.max(type_.Int, self.expr.type_)
        if self.type_ is None:
            self.error('type error')

    def gen(self)->expr.Expr:
        return Unary(self.op, self.expr.reduce())

    def __str__(self):
        return f"{(self.op)} {str(self.expr)}"