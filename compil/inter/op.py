import compil.inter.expr as expr
import compil.lexer_p.token as token
import compil.symbols_p.type as type_
import compil.inter.temp as temp

class Op(expr.Expr):
    def __init__(self, tok:token.Token, p:type_.Type):
        super().__init__(tok, p)

    def reduce(self)-> expr.Expr:
        x = self.gen()
        t = temp.Temp(self.type_)
        self.emit(f"{str(t)} = {str(x)}")
        return t
