import compil.inter.expr as expr
import compil.lexer_p.token as token
import compil.inter.logical as logical


class And(logical.Logical):

    def __init__(self, tok:token.Token, x1:expr.Expr, x2:expr.Expr):
        super().__init__(tok, x1, x2)
        
    def jumping(self, t:int, f:int):
        label:int = f if f != 0 else self.newlabel()
        self.expr1.jumping(0, label)
        self.expr2.jumping(t, f)
        if f == 0:
            self.emitlabel(label)