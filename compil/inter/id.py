import compil.inter.expr as expr
import compil.lexer_p.word as word
import compil.symbols_p.type as type_

class Id(expr.Expr):
    def __init__(self, id:word.Word, p:type_.Type, b:int):
        super().__init__(id, p)
        self.offset = b
