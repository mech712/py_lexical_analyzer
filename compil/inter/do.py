import compil.symbols_p.type as type_

import compil.inter.expr as expr
import compil.inter.stmt as stmt

class Do(stmt.Stmt):
    
    def __init__(self):
        self.expr = None
        self.stmt = None

    def init(self, x:expr.Expr, s:stmt.Stmt):
        self.expr = x
        self.stmt = s
        if self.expr.type_ != type_.Bool:
            self.expr.error("boolean required in do")
        
    def gen(self, b:int, a:int):
        self.after = a
        label:int = self.newlabel()
        self.stmt.gen(b, label)
        self.emitlabel(label)
        self.expr.jumping(b, 0)
