from ..lexer_p.lexer import Lexer
from ..lexer_p.token import Token
from ..lexer_p.tag import Tag
from ..lexer_p.word import Word
from ..lexer_p import word as word

from ..symbols_p.env import Env
from ..symbols_p.type import Type
from ..symbols_p import type as type_
from ..symbols_p.array import Array

from ..inter.stmt import Stmt
from ..inter import stmt
from ..inter.id import Id
from ..inter.seq import Seq
from ..inter.expr import Expr
from ..inter.if_ import If
from ..inter.else_ import Else
from ..inter.while_ import While
from ..inter.do import Do
from ..inter.break_ import Break
from ..inter.set import Set
from ..inter.access import Access
from ..inter.setelem import SetElem
from ..inter.or_ import Or
from ..inter.and_ import And
from ..inter.not_ import Not
from ..inter.rel import Rel
from ..inter.arith import Arith
from ..inter.unary import Unary
from ..inter.constant import Constant
from ..inter import constant as constant


class Parser:
    lex:Lexer = None
    look:Token = None
    top:Env = None
    used:int = 0

    def __init__(self, l:Lexer):
        self.lex = l
        self.move()

    def move(self):
        self.look = self.lex.scan()

    def error(self, s:str):
        raise Exception(f"near line {str(self.lex.line)}: {s}")

    def match(self, t:int):
        if self.look.tag == t:
            self.move()
        else:
            self.error("syntax error")

    def program(self):
        s:Stmt = self.block()
        begin:int = s.newlabel()
        after:int = s.newlabel()
        s.emitlabel(begin)
        s.gen(begin, after)
        s.emitlabel(after)

    def block(self)->Stmt:
        self.match('{')
        savedEnv:Env = self.top
        self.top = Env(self.top)
        self.decls()
        s:Stmt = self.stmts()
        self.match('}')
        self.top = savedEnv
        return s

    def decls(self):
        while self.look.tag == Tag.BASIC_:
            p:Type = self.type()
            tok:Token = self.look
            self.match(Tag.ID_)
            self.match(';')
            id:Id = Id(tok, p, self.used)
            self.top.put(tok, id)
            self.used = self.used + p.width

    def type(self):
        p:Type = self.look
        self.match(Tag.BASIC_)
        if self.look.tag != '[':
            return p
        else:
            return self.dims(p)

    def dims(self, p:Type)->Type:
        self.match('[')
        tok:Token = self.look
        self.match(Tag.NUM_)
        self.match(']')
        if self.look.tag == '[':
            p = self.dims(p)
        return Array(tok.value, p)

    def stmts(self)->Stmt:
        if self.look.tag == '}':
            return stmt.Null
        return Seq(self.stmt(), self.stmts())

    def stmt(self)->Stmt:
        x:Expr = None
        s:Stmt = None
        s1:Stmt = None
        s2:Stmt = None
        savedStmt:Stmt = None

        if self.look.tag == ';':
            self.move()
            return stmt.Null
        
        elif self.look.tag == Tag.IF_:
            self.match(Tag.IF_)
            self.match('(')
            x = self.bool()
            self.match(')')
            s1 = self.stmt()
            if self.look.tag != Tag.ELSE_:
                return If(x, s1)
            self.match(Tag.ELSE_)
            s2 = self.stmt()
            return Else(x, s1, s2)
        
        elif self.look.tag == Tag.WHILE_:
            whilenode:While = While()
            savedStmt = stmt.Enclosing
            stmt.Enclosing = whilenode
            self.match(Tag.WHILE_)
            self.match('(')
            x = self.bool()
            self.match(')')
            s1 = self.stmt()
            whilenode.init(x, s1)
            stmt.Enclosing = savedStmt
            return whilenode
        
        elif self.look.tag == Tag.DO_:
            donode:Do = Do()
            savedStmt = stmt.Enclosing
            stmt.Enclosing = donode
            self.match(Tag.DO_)
            s1 = self.stmt()
            self.match(Tag.WHILE_)
            self.match('(')
            x = self.bool()
            self.match(')')
            self.match(';')
            donode.init(s1, x)
            stmt.Enclosing = savedStmt
            return donode

        elif self.look.tag == Tag.BREAK_:
            self.match(Tag.BREAK_)
            self.match(';')
            return Break()

        elif self.look.tag == '{':
            return self.block()

        return self.assign()

    def assign(self)->Stmt:
        stmt:Stmt = None
        t:Token = self.look
        self.match(Tag.ID_)
        id:Id = self.top.get(t)
        if id is None:
            self.error(f"{str(t)} undeclared")
        if self.look.tag == '=':
            self.move()
            stmt = Set(id, self.bool())
        else:
            x:Access = self.offset(id)
            self.match('=')
            stmt = SetElem(x, self.bool())
        self.match(';')
        return stmt

    def bool(self)->Expr:
        x:Expr = self.join()
        while self.look.tag == Tag.OR_:
            tok:Token = self.look
            self.move()
            x = Or(tok, x, self.join())
        return x

    def join(self)->Expr:
        x:Expr = self.equality()
        while self.look.tag == Tag.AND_:
            tok:Token = self.look
            self.move()
            x = And(tok, x, self.equality())
        return x

    def equality(self)->Expr:
        x:Expr = self.rel()
        while self.look.tag == Tag.EQ_ or self.look.tag == Tag.NE_:
            tok:Token = self.look
            self.move()
            x = Rel(tok, x, self.rel())
        return x

    def rel(self)->Expr:
        x:Expr = self.rel()
        if self.look.tag == '<' or self.look.tag == Tag.LE_ or self.look.tag == Tag.GE_ or self.look.tag == '>':
            tok:Token = self.look
            self.move()
            return Rel(tok, x, self.expr())
        return x

    def expr(self)->Expr:
        x:Expr = self.term()
        while self.look.tag == '+' or self.look.tag == '-':
            tok:Token = self.look
            self.move()
            x = Arith(tok, x, self.term())
        return x

    def term(self)->Expr:
        x:Expr = self.unary()
        while self.look.tag == '*' or self.look.tag == '/':
            tok:Token = self.look
            self.move()
            x = Arith(tok, x, self.unary())
        return x

    def unary(self)->Expr:
        if self.look.tag == '-':
            self.move()
            return Unary(word.minus_, self.unary())
        elif self.look.tag =='!':
            tok:Token = self.look
            self.move()
            return Not(tok, self.unary())
        else:
            return self.factor()

    def factor(self)->Expr:
        x:Expr = None

        if self.look.tag == '(':
            self.move()
            x = self.bool()
            self.match(')')
            return x

        if self.look.tag == Tag.NUM_:
            x = Constant(self.look, type_.Int)
            self.move()
            return x

        if self.look.tag == Tag.REAL_:
            x = Constant(self.look, type_.Float)
            self.move()
            return x

        if self.look.tag == Tag.TRUE_:
            x = constant.True_
            self.move()
            return x

        if self.look.tag == Tag.FALSE_:
            x = constant.False_
            self.move()
            return x

        if self.look.tag == Tag.ID_:
            s:str = str(self.look)
            id:Id = self.top.get(self.look)
            if id is None:
                self.error(f"{str(self.look)} undeclared")
            self.move()
            if self.look.tag != '[':
                return id
            else:
                return self.offset(id)

        self.error('syntax error')
        return x

    def offset(self, a:Id)->Access:
        i:Expr = None
        w:Expr = None
        t1:Expr = None
        t2:Expr = None
        loc:Expr = None
        
        tp:Type = a.type_
        self.match('[')
        i = self.bool()
        self.match(']')
        tp = tp.of
        w = Constant(tp.width)
        t1 = Arith(Token('*'), i, w)
        loc = t1

        while self.look.tag == '[':
            self.match('[')
            i = self.bool()
            self.match(']')
            tp = tp.of
            w = Constant(tp.width)
            t1 = Arith(Token('*'), i, w)
            t2 = Arith(Token('+'), loc, t1)
            loc = t2
        return Access(a, loc, tp)