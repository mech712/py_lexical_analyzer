import compil.lexer_p.lexer as lexer
import compil.parser.parser as parser

if __name__ == "__main__":
    with open('text', 'r') as f:
        text = f.read()

    inp = lexer.Input()
    inp.buffer = text

    lex = lexer.Lexer(inp=inp)
    parse = parser.Parser(l=lex)
    parse.program()
    print('\n')